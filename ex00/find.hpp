//
// find.hpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d17/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 24 12:23:23 2014 Jean Gravier
// Last update Fri Jan 24 13:13:45 2014 Jean Gravier
//

#ifndef _FIND_H_
#define _FIND_H_

#include <algorithm>
#include <iterator>
#include <vector>

template <typename T>
typename T::iterator		do_find(T &temp, int index)
{
  return (std::find(temp.begin(), temp.end(), index));
}

#endif /* _FIND_H_ */
